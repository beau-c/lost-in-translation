import { createContext, useContext, useState } from "react";
import { storageLoad } from "../utils/storage";
import { STORAGE_KEY_USER } from "../constants/storageKeys";

const UserContext = createContext();

export const useUser = () => {
  return useContext(UserContext);
};

const UserProvider = ({ children }) => {

  const [user, setUser] = useState(storageLoad(STORAGE_KEY_USER));

  const state = {
    user,
    setUser
  };

  return (
    <UserContext.Provider value={ state }>
      { children }
    </UserContext.Provider>
  );
};

export default UserProvider;
