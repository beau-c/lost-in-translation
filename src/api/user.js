/**
 * API users functions
 *
 * Note: The API calls were purposefully not wrapped inside try/catch blocks here.
 * (I prefer to handle errors outside of these functions)
 *
 * @author Beau C
 **/
import { createHeaders } from "./index";
import { nanoid } from "nanoid";

const API_URL = `${process.env.REACT_APP_API_URL}/translations`;

// Returns any users matching the given username
export const userGet = async (username) => {
  const response = await fetch(`${API_URL}?username=${username}`);

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }
  return await response.json()
}

// Creates a new user and returns it
export const userCreate = async (username) => {
  const response = await fetch(`${API_URL}`, {
    method: "POST",
    headers: createHeaders(),
    body: JSON.stringify({
      id: nanoid(),
      username: username.toLowerCase(),
      translations: []
    })
  });

  if (!response.ok) {
    throw new Error("Unable to create a new user.");
  }
  return await response.json();
}

// Updates an existing user
export const userUpdate = async (user) => {
  const response = await fetch(`${API_URL}/${user.id}`, {
    method: "PATCH",
    headers: createHeaders(),
    body: JSON.stringify(user)
  });

  if (!response.ok) {
    throw new Error("Unable update user.");
  }

  return await response.json();
}

// Temporary login function, creates a new user if necessary
export const userLogin = async (username) => {
  const user = await userGet(username);

  if (user.length > 0) {
    return user.pop();
  }
  return userCreate(username);
}
