import { useState, useEffect } from "react"
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { userLogin } from "../../api/user";
import { storageSave } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../constants/storageKeys";
import { useUser } from "../../context/UserContext"

const usernameConfig = {
  required: true,
  minLength: 4
};

const LoginForm = () => {

  const { register, handleSubmit, formState: { errors } } = useForm();

  const { user, setUser } = useUser();

  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);
  const [loginError, setLoginError] = useState(null);

  useEffect(() => {
    if (user !== null) {
      navigate("/profile");
    }
  }, [user, navigate]);

  const onSubmit = async ({ username }) => {
    if (username.trim() === "") {
      setLoginError("Username cannot be empty!");
      return;
    }

    setLoading(true);

    try {
      const userResponse = await userLogin(username);
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    catch(ex) {
      setLoginError(ex.message);
    }
    finally {
      setLoading(false);
    }
  }

  const usernameErrorMessage = () => {
    switch(errors.username.type) {
      case "required":
        return <span>A username is required.</span>
      case "minLength":
        return <span>Username must have {usernameConfig.minLength} or more characters.</span>
      default:
        return null;
    }
  }

  return (
    <>
      <p>Please log in to continue.</p>
      { loginError && <p Style="color:red;">{loginError}</p>}
      <form onSubmit={ handleSubmit(onSubmit) }>
      <div className="row">
        <fieldset>
        <label htmlFor="username">Username:</label>
        <input className="mx-2" type="text" { ...register("username", usernameConfig) } />
          { errors && errors.username && usernameErrorMessage() }
        </fieldset>
      </div>
      <div className="row mt-2">
        <div className="col-md-12">
          {
            (loading && <div className="lds-dual-ring black"></div>) || (<button className="btn btn-primary" type="submit" disabled = {loading}>Log in</button>)
          }
        </div>
      </div>
      </form>
    </>
  )
}

export default LoginForm;
