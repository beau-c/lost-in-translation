import { useState } from "react"
import { useSearchParams } from "react-router-dom";
import { storageSave } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../constants/storageKeys";
import { useUser } from "../../context/UserContext"
import { userUpdate } from "../../api/user";
import BootstrapSwitchButton from 'bootstrap-switch-button-react'
import Icon from '@mdi/react';
import { mdiArrowDownBold } from '@mdi/js';

let currentInput = ""; // Represents the text that is currently (being) typed

const TranslatorInput = ({translate}) => {

  const { user, setUser } = useUser();

  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);
  const [searchParams] = useSearchParams();
  const [hasInputChanged, setHasInputChanged] = useState(searchParams.has("i"));
  const [toggleLive, setToggleLive] = useState(false);

  if (currentInput === "" && searchParams.has("i")) {
    // If there is an initial value in the query parameters, set it to the current input.
    currentInput = searchParams.get("i");
  }

  const onInputChange = (event) => {
    // When the input textarea changes, update the value of currentInput
    const originalInput = event.target.value;
    currentInput = originalInput;
    if (toggleLive) {
      // Only translate the text right away if live translation is toggled ON.
      translateText(originalInput);
    }
    setHasInputChanged(true);
  }

  // Toggling live translation ON/OFF
  const onToggleLive = (checked) => {
    setToggleLive(checked);
  }

  const translateText = async (input) => {
    if (input.trim() === "") // ignore empty input
      return;

    // If live translation is NOT toggled, we should store this translation in history before we translate.
    if (!toggleLive) {
      // Store this translation in the API.
      setLoading(true);
      setApiError("");

      try {
        const updatedUser = await userUpdate({ ...user, translations: [...user.translations, input] });
        storageSave(STORAGE_KEY_USER, updatedUser);
        setUser(updatedUser);
      }
      catch(exception) {
        setApiError(exception.message);
      }
      finally {
        setLoading(false);
      }
    }

    // Filter out all the words and pass the input to our parent.
    const inputWords = input.match(/[a-zA-z]+/g);
    translate(inputWords);
    setHasInputChanged(false);
  }

  return (
    <>
      { apiError && <p className="text-danger">{apiError}</p> }
      <div className="row">
          <div className="col-md-12">
            <textarea rows="5" cols="50" placeholder="Enter your text to be translated here." onChange={onInputChange} defaultValue={searchParams.get("i") ?? ""} />
          </div>
        </div>
      <div className="row">
        <div className="col-md-12">
          <span>Live translation: <BootstrapSwitchButton checked={toggleLive} size="sm" onstyle="danger" offstyle="outline-secondary" onChange={ (checked) => onToggleLive(checked) } /></span>
          <div className="red-blob" style={{visibility: toggleLive ? "visible" : "hidden"}}></div>
          <span className="text-information" style={{visibility: toggleLive ? "visible" : "hidden"}}><br/>(During live translation your history will not be saved.)</span>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <button className="btn btn-primary btn-translate" disabled={toggleLive || !hasInputChanged} onClick={ () => translateText(currentInput) }>
          { (!loading && <span>Translate <br/><Icon path={mdiArrowDownBold} size={1} title="Translate" /></span>) || (<div class="lds-dual-ring"></div>)}
          </button>
        </div>
      </div>
    </>
  )
}

export default TranslatorInput;
