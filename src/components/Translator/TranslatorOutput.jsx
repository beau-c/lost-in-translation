const TranslatorOutput = ({ inputWords }) => {

  return (
    <div className="translationOutput">
      {
        inputWords &&
        inputWords.map((word, i) => {
          return <div key={i} className="singleWord">
            { /* Display each input word separately */ }
            <div className="singleWordText">{word}</div>
            {
              // Render the corresponding ASL image for each individual character in the word
              word.split("").map( (char, i) => {
                const isUpper = char === char.toUpperCase();
                return <img key={i} className={isUpper ? "signImage upperCase" : "signImage"} src={`images/asl/${char.toLowerCase()}.png`} title={char} alt={char} />
              })
            }
          </div>
        })
      }
    </div>
  )
}

export default TranslatorOutput;
