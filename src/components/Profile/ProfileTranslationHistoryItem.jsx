import Icon from '@mdi/react';
import { mdiTransition } from '@mdi/js';

const ProfileTranslationHistoryItem = ({ translation, key }) => {
  return (<li key={key} className="list-group-item">
    <span className="mx-2">{translation}</span>
      <a className="btn btn-outline-primary" href={`/translator?i=${translation}`}>
      <Icon path={mdiTransition} size={1} title="Translator" /> Edit in translator
    </a>
  </li>);
}

export default ProfileTranslationHistoryItem;
