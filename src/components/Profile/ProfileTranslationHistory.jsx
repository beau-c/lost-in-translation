import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";

const ProfileTranslationHistory = ({ translations }) => {

  return (
    <section>
        <h4>Your translations</h4>
        <div className="container">
        {
          (translations.length === 0 && <p className="text-information">You don't have any stored translations.</p>)
          ||
          (
            <ul className="list-group">
            {
              translations.slice(0).reverse().slice(0,10).map( (translation, i) => {
                  return <ProfileTranslationHistoryItem translation={translation} key={i} />
                }
              )
            }
            </ul>
          )
        }
        </div>
    </section>
  );
};
export default ProfileTranslationHistory;
