import Icon from '@mdi/react';
import { useState } from "react"
import { mdiTrashCan } from '@mdi/js';
import { useUser } from "../../context/UserContext";
import { userUpdate } from "../../api/user";
import { storageSave } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../constants/storageKeys";

const ProfileActions = () => {

  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);
  const { user, setUser } = useUser();

  const onClickClearHistory = async () => {
    // Updates the current user, clears its translations.
    // (obviously should be done differently in a real application(!))
    setLoading(true);
    setApiError("");
    try {
      const updatedUser = await userUpdate(
        {
          ...user,
          translations: []
        }
      );
      storageSave(STORAGE_KEY_USER, updatedUser);
      setUser(updatedUser);
    }
    catch(error) {
      setApiError(error.message);
    }
    finally {
      setLoading(false);
    }
  }

  return (
    <div>
      { apiError && <p className="text-danger">{apiError}</p> }
      {
        (loading && <div className="lds-dual-ring black"></div>)
        ||
        (
          <button className="btn btn-outline-danger" onClick={ () => onClickClearHistory() }>
            <Icon path={mdiTrashCan} size={1} title="Clear History" /> Clear Translation History
          </button>
        )
      }
    </div>
  )
}

export default ProfileActions;
