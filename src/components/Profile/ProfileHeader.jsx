const ProfileHeader = ({ username }) => {
  return (
    <header>
      <h2>Welcome, {username}.</h2>
      <img className="logo-big" src="images/Logo-Hello.png" alt="logo" />
    </header>
  );
};

export default ProfileHeader;
