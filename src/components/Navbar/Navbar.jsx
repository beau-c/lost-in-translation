import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../constants/storageKeys";
import { storageRemove } from "../../utils/storage";

import Icon from '@mdi/react';
import { mdiAccountCircle, mdiTransition, mdiLogoutVariant } from '@mdi/js';

const Navbar = () => {

  const { user, setUser } = useUser();

  const handleLogout = () => {
    if (window.confirm("Are you sure you want to logout?")) {
      storageRemove(STORAGE_KEY_USER);
      setUser(null);
    }
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
            <a className="navbar-brand" href="/">
              <img alt="logo" class="nav-logo" src="images/Logo.png" />
              Lost In Translation
            </a>
            <div className="collapse navbar-collapse" id="navbarContent">
                { user !== null &&
                  <>
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                          <a className="btn btn-primary" href="/translator">
                          <span><Icon path={mdiTransition} size={1} title="Translator" /> ASL Translator</span>
                          </a>
                        </li>
                    </ul>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                          <a className="btn btn-light" href="/profile">
                          <span><Icon path={mdiAccountCircle} size={1} title="User Profile" /> {user.username}</span>
                          </a>
                        </li>
                        <li className="nav-item">
                         <button className="btn btn-danger" onClick={ handleLogout }>
                          <span><Icon path={mdiLogoutVariant} size={1} title="Logout" /> Logout</span>
                         </button>
                        </li>
                    </ul>
                  </>
                }
            </div>
        </div>
    </nav>
  )
}

export default Navbar;
