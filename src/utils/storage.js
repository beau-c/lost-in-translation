export const storageSave = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
}

export const storageLoad = (key) => {
  const value = localStorage.getItem(key);
  if (value !== null) {
    return JSON.parse(value);
  }
  return null;
}

export const storageRemove = (key) => {
  localStorage.removeItem(key);
}
