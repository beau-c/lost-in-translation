import LoginForm from '../components/Login/LoginForm';

const Login = () => {
  return (
    <>
      <h1>Lost In Translation</h1>
      <img class="logo-big" src="images/Logo.png" alt="logo" />
      <LoginForm />
    </>
  );
}

export default Login;
