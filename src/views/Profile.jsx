import withAuth from "../hoc/withAuth";
import { useUser } from "../context/UserContext";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";

const Profile = () => {

  const { user } = useUser();

  return (
    <>
      <div className="row">
        <div className="col-md-12">
          <ProfileHeader username={ user.username } />
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <ProfileTranslationHistory translations={ user.translations } />
          <ProfileActions />
        </div>
      </div>
    </>
  );
}

export default withAuth(Profile);
