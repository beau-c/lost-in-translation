import withAuth from "../hoc/withAuth";
import { useState } from "react"
import TranslatorInput from "../components/Translator/TranslatorInput";
import TranslatorOutput from "../components/Translator/TranslatorOutput";

const Translator = () => {

  const [inputWords, setInputWords] = useState([]);

  // Used to pass translation state between components.
  const translate = (input) => {
    setInputWords(input);
  }

  return (
    <div className="container-fluid">
      <h1>ASL Translator</h1>
      <TranslatorInput translate={translate} />
      <TranslatorOutput inputWords={inputWords} />
    </div>
  );
}

export default withAuth(Translator);
