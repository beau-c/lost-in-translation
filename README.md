# Assignment 2
This repository contains a submission for the second assignment, a React app for translating text to American Sign Language (ASL).

## Instructions
Run 
```console
npm install && npm start
```

## Usage
The application contains a translator, which can translate text into ASL.
Translations will translate letters from A to Z to sign language and will be stored in history.
There is also an option for realtime translating while typing.

Users can log in, translate, view/clear translation history and log out.

## Screenshots
![](https://gitlab.com/beau-c/lost-in-translation/-/raw/master/public/documents/l-i-t-1.png)

## Component tree
![](https://gitlab.com/beau-c/lost-in-translation/-/raw/master/public/documents/lost-in-translation_component-tree.png)

## License
#### GNU GPL v3
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
